[TOC]

![](img/my-app-01.PNG)

# Antes de empezar, requisitos previos

Antes de comenzar necesitarás tener instalados [**Node.js**](https://nodejs.org/es/) (versión 8.x or 10.x, se puede comprobar con `node -v`) y el gestor de paquetes [**npm**](https://www.npmjs.com/) (`npm -v`).

# Instalar Angular CLI

**Angular** cuenta con un interface de línea de comandos para crear nuestros proyectos, para instalarlo ejecutar:

```JS
npm install -g @angular/cli
```

# Creación del entorno de trabajo

Ahora procedemos a crear un nuevo espacio de trabajo que puede albergar más de un proyecto.

```JS
ng new my-app
```

Durante la creación del espacio de trabajo el comando `ng new` solicita algunos datos sobre las características de nuestra aplicación, aceptamos la opciones por defecto pulsando _enter_.

Después de unos minutos ya tenemos el espacio de trabajo en la carpeta "new-app" creado con todos los paquetes npm y dependencias, dentro en el directorio "src" tendremos un esqueleto de proyecto inicial.

# Ejecutar la aplicación

```JS
cd my-app
ng serve --open
```

El comando `ng serve` ejecuta el servidor y recompila la aplicación refrescando el contenido de la aplicación Web a medida que realizamos cambios en los ficheros. EL parámetro `--open` abre la aplicación en el navegador ([http://localhost:4200/](http://localhost:4200/)).

![](img/my-app-02.PNG)

Debemos dejar la consola abierta, cualquier cambio que realicemos en el proyecto se visibilizará en el navegador de forma automática.

# Estructura de un proyecto

Cuando creamos un nuevo espacio de trabajo **Angular** genera una estructura de carpetas y crea un montón de archivos (mi carpeta "my-app" ocupa 256MB en disco y tiene 26.634 archivos), parece algo desproporcionado para una aplicación tan sencilla y en la mayoría de los casos no modificaremos la mayoría de los archivos que forman el proyecto. Vamos a repasar por encima el propósito de algunos de estos ficheros y como se estructura el proyecto.

![](img/my-app-03.PNG)

La carpeta "node_modules" alberga los módulos Node.js, la carpeta "src" es donde trabajaremos sobre nuestra aplicación la mayor parte del tiempo.

El archivo "angular.json" apunta a "main.ts" que es el punto de entrada de ejecución de nuestra aplicación, "index.html" es la página que contendrá toda nuestra aplicación SPA, entre otros parámetros también hace referencia a la hoja de estilos CSS que usamos (`"styles": ["src/styles.css"],`).

![](img/my-app-04.PNG)

El archivo "package.json" define las dependencias que tiene nuestro proyecto.

![](img/my-app-05.PNG)

Dentro de la carpeta "src/app" podemos encontrar los componentes que forman la página.

![](img/my-app-06.PNG)

El "src/index.html" es la Web que contiene toda nuestra aplicación SPA.

![](img/my-app-07.PNG)

Si os fijáis bien la etiqueta `<app-root></app-root>` no es estándar, esto lo entiende solamente **Angular** y despliega los componentes que contiene la carpeta "src/app/", todos los archivos con extensión .ts se refieren evidentemente a código TypeScript.

# Editando un componente de Angular

El archivo "src/app/app.component.html" contiene el HTML que visualizamos. Vamos a cambiar el mensaje:

![](img/my-app-08.PNG)

```JS
<h1>Bienvenido a {{ title }}!</h1>
```

Y vamos al navegador para ver como cambia el mensaje:

![](img/my-app-09.PNG)

También vamos a quitar (o comentar) el título de segundo nivel y los enlaces de abajo por ejemplo para ver como se renderiza el resultado:

![](img/my-app-10.PNG)

La propiedad `{{ title }}` es interpretada por **Angular**, el contenido que lo sustituye lo obtiene de /src/app/app.component.ts de la clase `AppComponent`, cambiamos el texto para realizar la prueba:

```JS
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "mi primera app en Angular";
}
```

Resultado:

![](img/my-app-11.PNG)

El archivo /src/app/app.component.spec.ts podemos eliminarlo por el momento, se utiliza para realizar pruebas. /src/app/app.module.ts se utiliza para inicializar nuestra aplicación pero lo veremos más adelante. De momento el archivo CSS /src/app/app.component.css está vacio.

La clase `AppComponent` se exporta usando la palabra clave `export` para que pueda ser usada por el _framework_ de angular en otros archivos, la variable `title` es la única propiedad de la clase por el momento. Si quisiésemos podríamos cambiar el nombre de la variable y también evidentemente en /src/app/app.component.html `<h1>Bienvenido a {{ titulo }}!</h1>`.

```JS
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  titulo = "mi primera app en Angular";
}
```

# Siguientes pasos

- Basado en el tutorial oficial [Tour of Heroes tutorial](../tour-of-heroes/tour-of-heroes.md)

# Código fuente

- angular / src / 01-intro [my-app](https://gitlab.com/soka/angular/tree/master/src/01-intro/my-app).

# Enlaces internos

- ["Recarga los cambios de tu página HTML/JS/CSS de forma automática - NodeJS Live Server"](https://soka.gitlab.io/blog/post/2019-03-17-npm-live-server/).

# Enlaces externos

- [angular.io/guide/quickstart](https://angular.io/guide/quickstart).
- [https://stackblitz.com/](https://stackblitz.com/)
