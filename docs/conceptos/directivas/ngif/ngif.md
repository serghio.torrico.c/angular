[TOC]

# Ejemplo básico ngIf

Voy a mostrar un mensaje condicionado a la pulsación de un botón.

Vista:

```ts
<div class="container">
  <div class="row">
    <div class="col">
      <label>Título de persona</label>
      <input type="text" class="form-control" [(ngModel)]="tituloPersona" />
      <button class="btn btn-primary" (click)="onCrearPersona()">
        Agregar persona
      </button>
      <p *ngIf="personaCreada">{{ agregarPersonaStatus }}</p>
    </div>
  </div>
</div>
```

Componente:

```ts
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Directiva *ngIf";
  tituloPersona;
  agregarPersonaStatus: string = "Persona agregada";
  personaCreada: boolean = false;

  onCrearPersona(): void {
    console.log("onCrearPersona");
    this.personaCreada = true;
  }
}
```

![](img/01.gif)

Proyecto en Stackblitz [ngif-app-ex-01](https://stackblitz.com/edit/ngif-app-ex-01):

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/ngif-app-ex-01?embed=1&file=src/app/app.component.html"></iframe>
