[TOC]

Sigo con el proyecto de ejemplo que empecé con "Crear un componente con Angular CLI". En lugar de definir la plantilla en "personas.component.html" voy a definir todo el contenido dentro de la definición del componente, reemplazo la línea `templateUrl: './personas.component.html'` por:

```ts
@Component({
  selector: "app-personas",
  template: `
    <h2>Listado de personas</h2>
    <app-persona></app-persona>
    <app-persona></app-persona>
  `,
  styleUrls: ["./personas.component.css"]
})
```

Para facilitar la legibilidad usamos "`" (se conoce como _backtick_ en inglés) que nos permite saltar entre líneas. Podemos hacer lo mismo con los estilos CSS.

Está práctica sólo es recomendable cuando tenemos pocas líneas de código en nuestra plantilla.

# Código fuente del ejemplo

- angular / src / conceptos / componentes / [new-component-cli](https://gitlab.com/soka/angular/tree/master/src/conceptos/componentes/new-component-cli).

# Enlaces externos

- ["Angular - Component Styles"](https://angular.io/guide/component-styles).
