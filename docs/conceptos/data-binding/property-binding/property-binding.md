[TOC]

# Vinculación de propiedades ([property])

En enlazado de propiedades se utiliza la sintaxis `[propiedad]="expresion"`. La forma más común de usarlo es vincular una propiedad del componente a un atributo de una etiqueta:

```ts
import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  template: `
    <img [src]="miImagen" />
  `
})
export class AppComponent {
  title = "property-binding";
  miImagen = "assets/img/keepcalm.PNG";
}
```

Otro ejemplo, deshabilitar un botón:

```ts
    <button [disabled]="isDisabled">Cancel is disabled</button>
```

Propiedad del componente:

```ts
isDisabled = true;
```

Su equivalente HTML:

```html
<button disabled>HTML Cancel is disabled</button>
```

# Habilitar botón

Añado el constructor de la clase con este código, usando la función flecha transcurridos tres segundos en botón se habilita.

```ts
  constructor() {
    setTimeout(() => {
      this.isDisabled = false;
    }, 3000);
  }
```

# Código fuente

- angular / src / conceptos / data-binding / [property-binding](https://gitlab.com/soka/angular/tree/master/src/conceptos/data-binding/property-binding).

<iframe width="100%" height="600px" src="https://stackblitz.com/edit/property-bind?embed=1&file=src/app/app.component.ts"></iframe>

# Enlaces externos
