import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";

import { ButtonModule } from "primeng/button"; // <----
import { PasswordModule } from "primeng/password";
import { TableModule } from "primeng/table";

@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, ButtonModule, PasswordModule, TableModule], // <----
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
