import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-persona",
  templateUrl: "./persona.component.html",
  styleUrls: ["./persona.component.css"]
})
export class PersonaComponent implements OnInit {
  nombrePersona: string = "Iker";
  apellidoPersona: string = "Landajuela";
  edadPersona: number = 40;
  miArray: string[] = ["Uno", "Dos", "Tres"];

  private alturaPersona: number = 1.7;

  getAlturaPersona(): number {
    return this.alturaPersona;
  }

  constructor() {}

  ngOnInit() {}
}
