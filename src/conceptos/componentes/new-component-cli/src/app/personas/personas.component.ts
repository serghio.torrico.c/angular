import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-personas",
  template: `
    <h2>Listado de personas</h2>
    <app-persona></app-persona>
    <app-persona></app-persona>
  `,
  styleUrls: ["./personas.component.css"]
})
export class PersonasComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
