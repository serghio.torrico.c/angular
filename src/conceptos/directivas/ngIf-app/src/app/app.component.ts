import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "Directiva *ngIf";
  tituloPersona;
  agregarPersonaStatus: string = "Persona agregada";
  personaCreada: boolean = false;

  onCrearPersona(): void {
    console.log("onCrearPersona");
    this.personaCreada = true;
  }
}
